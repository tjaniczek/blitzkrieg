const webpack = require('webpack');
const WebpackDevServer = require('webpack-dev-server');
const config = require('./webpack.config');

const compiler = webpack(config);
const server = new WebpackDevServer(compiler, {
  publicPath: config.output.publicPath,
  historyApiFallback: true,
  stats: { colors: true }
})

server.listen(3000, '0.0.0.0', function (err, result) {
  console.log(err ? err : 'Listening at 0.0.0.0:3000')
});
