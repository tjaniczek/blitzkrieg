import Coach from './Coach';
import { CoordinatesMap } from './Coordinates';
import { PieceRelation } from './types'

export default class CoachTester extends Coach {
  constructor(fenNotation) {
    super(fenNotation);
  }

  fieldOn(field) {
    const coordinates = CoordinatesMap[field];
    return this.board[coordinates.row][coordinates.file];
  }

  pieceOn(field) {
    return this.fieldOn(field).piece;
  }

  relationTo(field) {
    const related = this.fieldOn(field);
    const relation:PieceRelation = {
      row: related.row,
      file: related.file,
      piece: related.piece,
    }
    return relation;
  }

  p() {
    console.log('\/\/ ' + this.getAscii().split('\n').join('\n\/\/ ').substr(0, 358))
  }
}
