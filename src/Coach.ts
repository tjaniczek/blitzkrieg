import { Chess } from 'chess.js';
import Board from './Board';
import {
  Piece,
  Pieces,
  Direction,
} from './Pieces';
import Pathfinder from './Pathfinder';
import XRay from './XRay';
import {
  Field,
  SpecialMoves,
  PieceRelation,
  XRayRelation,
} from './types';
import { CoordinatesMap } from './Coordinates';

export default class Coach extends Board {
  private specialMoves:SpecialMoves = {
    enPassant: undefined,
    whiteCastling: {
      kingSide: undefined,
      queenSide: undefined,
    },
    blackCastling: {
      kingSide: undefined,
      queenSide: undefined,
    },
  };

  public xRay: any;

  constructor(fenNotation:string) {
    super(fenNotation);
    this.annotateSpecialMovesFromFen(fenNotation);

    this.xRay = new XRay(this);
  }

  annotateSpecialMovesFromFen(fenNotation:string) {
    const valid = this.chessJs.validate_fen(fenNotation);
    if (!valid) {
      throw new Error('Invalid FEN notation');
    }

    // TODO: may be better to use only en passant and castlings
    const [
      position,
      activePiece,
      castlings,
      enPassant,
      halfMove,
      fullMove ] = fenNotation.split(' ');

    this.annotateCastlingsFromFen(castlings);
    this.annotateEnPassantFromFen(enPassant);
  }

  annotateCastlingsFromFen(castlingNotation:string) {
      // TODO: Write tests for this
      if (castlingNotation === '-') {
        return undefined;
      }

      this.specialMoves.whiteCastling = {
        kingSide: castlingNotation.includes('K'),
        queenSide: castlingNotation.includes('Q'),
      };
      this.specialMoves.blackCastling = {
        kingSide: castlingNotation.includes('k'),
        queenSide: castlingNotation.includes('q'),
      };
  }

  annotateEnPassantFromFen(enPassantNotation:string) {
      // TODO: when adding a move annotate en passant square:
      // https://chessprogramming.wikispaces.com/En+passant
      if (enPassantNotation === '-') {
        return undefined;
      }
      this.specialMoves.enPassant = { ...CoordinatesMap[enPassantNotation] };
  }

  annotate() {
    const addAttacks = fieldInfo => this.annotatePseudoAttacks(fieldInfo);
    this.board = Pathfinder.forEveryField(this.board, addAttacks);
    // ADD PINS WITH ANNOTATE ATTACKS FTW!

    const removeAttacks = fieldInfo => this.removePinnedAttacks(fieldInfo);
    this.board = Pathfinder.forEveryField(this.board, removeAttacks);

    // const addTactics = fieldInfo => this.annotateTactics(fieldInfo);
    // this.board = Pathfinder.forEveryField(this.board, addTactics);

    // TODO: remove pins before annotating double attacks
  }

  annotatePseudoAttacks({ field }) {
    if (!field.piece) {
      return field;
    }

    field.attacks = [];

    const type:number = field.piece.id;
    switch (type) {
    case Piece.Pawn:
      this.annotatePawnAttacks(field);
      break;
    case Piece.Knight:
      this.annotateKnightAttacks(field);
      break;
    case Piece.Bishop:
      this.annotateBishopAttacks(field);
      break;
    case Piece.Rook:
      this.annotateRookAttacks(field);
      break;
    case Piece.Queen:
      this.annotateQueenAttacks(field);
      break;
    case Piece.King:
      this.annotateKingAttacks(field);
      break;
    default:
      throw new Error('Unrecognized piece type');
    }
    return field;
  }

  annotatePawnAttacks(attacker:Field):Field {
    const piece = attacker.piece;
    const attackRow:number = this.getPawnAttackRow(attacker);

    if (attacker.file !== 0) {
      const attackFile:number = attacker.file - 1;
      const target = this.board[attackRow][attackFile];
      this.annotateRelation(attacker, target);
      this.annotateEnPassantAttack(attacker, attackRow, attackFile);
    }

    if (attacker.file !== 7) {
      const attackFile:number = attacker.file + 1;
      const target:Field = this.board[attackRow][attackFile];
      this.annotateRelation(attacker, target);
      this.annotateEnPassantAttack(attacker, attackRow, attackFile);
    }

    return attacker;
  }

  getPawnAttackRow(attacker):number {
    let row:number = attacker.row;
    // White pieces attack row above in the array
    return attacker.piece.color === 'w' ? row - 1 : row + 1;
  }

  annotateRelation(attacker:Field, target:Field) {
    const targetCoordinates:PieceRelation = {
      row: target.row,
      file: target.file,
      piece: target.piece,
    };

    const attackCoordinates:PieceRelation = {
      row: attacker.row,
      file: attacker.file,
      piece: attacker.piece,
    };

    if (this.isDefending(attacker, target)) {
      target.defenders.push(attackCoordinates);
      attacker.defends.push(targetCoordinates);
    } else {
      target.attackers.push(attackCoordinates);
      attacker.attacks.push(targetCoordinates);
    }
  }

  isDefending(attacker:Field, target:Field) {
    return target.piece &&
           target.piece.color == attacker.piece.color;
  }

  annotateEnPassantAttack(attacker:Field, attackRow:number, attackFile:number) {
    const enPassant = this.specialMoves.enPassant;
    if (!enPassant) {
      return undefined;
    }

    // TODO: En passant special cases:
    // - when taken piece reveals one of the kings
    // - when moved piece reveals one of the kings
    // - row after attack ( https://chessprogramming.wikispaces.com/En+passant )
    // - when en passant is an easy target:
    // - en passant will have two attackers but only one can attack
    // - en passant may not be defended
    // - ...but the final field may be attacked (wtf)

    // ALSO, add enPassantTarget and test for this and enPassant tactic!!

    // May be easier to just try the move in Chess.js

    if (attackRow === enPassant.row &&
        attackFile === enPassant.file) {

      const targetRow = attacker.row;
      const targetFile = attackFile;
      const target:Field = this.board[targetRow][targetFile];
      const targetCoordinates:PieceRelation = {
        row: targetRow,
        file: targetFile,
        piece: target.piece,
      };

      const attackCoordinates:PieceRelation = {
        row: attacker.row,
        file: attacker.file,
        piece: attacker.piece,
      };

      target.attackers.push(attackCoordinates);
      attacker.attacks.push(targetCoordinates);
      attacker.tactics.enPassant = true;
    }
  }

  annotateKnightAttacks(attacker) {
      this.getKnightTargets(attacker).map(
        target => this.annotateKnightTarget(attacker, target)
      );
  }

  annotateKnightTarget(attacker, target) {
    const attackCoordinates:PieceRelation = {
      row: attacker.row,
      file: attacker.file,
      piece: attacker.piece,
    };

    const targetCoordinates:PieceRelation = {
      row: target.row,
      file: target.file,
      piece: target.piece,
    };

    if (target.piece && target.piece.color === attacker.piece.color) {
      attacker.defends.push(targetCoordinates);
      target.defenders.push(attackCoordinates);
    } else {
        attacker.attacks.push(targetCoordinates);
        target.attackers.push(attackCoordinates);
    }
  }

  getKnightTargets(field) {
    const base = [field.row, field.file];
    const targets = [
      [2, 1], [2, -1], [-2, 1], [-2, -1],
      [1, 2], [1, -2], [-1, 2], [-1, -2],
    ];

    return targets.map(offset => [base[0] + offset[0], base[1] + offset[1]])
                  .filter(offset => this.isOffsetOnBoard(offset[0], offset[1]))
                  .map(offset => this.board[offset[0]][offset[1]])
  }

  isOffsetOnBoard(x: Number, y:Number):Boolean {
    return (x >= 0 && x < 8) && (y >= 0 && y < 8);
  }

  annotateBishopAttacks(field) {
    this.annotateRightDiagonalAttack(field);
    this.annotateLeftDiagonalAttack(field);
    return field;
  }

  annotateRookAttacks(field) {
    this.annotateFileAttack(field);
    this.annotateRowAttack(field);

    // TODO: Two pawns on the same row may both be pinned if en passant is possible
    // https://chessprogramming.wikispaces.com/En+passant
    return field;
  }

  annotateQueenAttacks(field) {
    this.annotateRightDiagonalAttack(field);
    this.annotateLeftDiagonalAttack(field);
    this.annotateFileAttack(field);
    this.annotateRowAttack(field);

    // TODO: Two pawns on the same row may both be pinned if en passant is possible
    // https://chessprogramming.wikispaces.com/En+passant
    return field;
  }

  annotateRightDiagonalAttack(field) {
      const rightDiagonal:Field[] = Pathfinder.getRightDiagonal(this.board, field.row, field.file);
      const rightIndex = rightDiagonal.indexOf(field);
      if (rightIndex !== 0) {
        const vector = rightDiagonal.slice(0, rightIndex).reverse();
        this.xRay.annotate(field, vector, Direction.RightDiagonal);
      }

      if (rightIndex !== 7) {
        const vector = rightDiagonal.slice(rightIndex + 1);
        this.xRay.annotate(field, vector, Direction.RightDiagonal);
      }
  }

  annotateLeftDiagonalAttack(field) {
    const leftDiagonal:Field[] = Pathfinder.getLeftDiagonal(this.board, field.row, field.file);
    const leftIndex = leftDiagonal.indexOf(field);
    if (leftIndex !== 0) {
      const vector = leftDiagonal.slice(0, leftIndex).reverse();
      this.xRay.annotate(field, vector, Direction.LeftDiagonal);
    }

    if (leftIndex !== 7) {
      const vector = leftDiagonal.slice(leftIndex + 1);
      this.xRay.annotate(field, vector, Direction.LeftDiagonal);
    }
  }

  annotateRowAttack(field) {
    const rowIndex = field.row;
    const fileIndex = field.file;
    const row:Field[] = Pathfinder.getRow(this.board, rowIndex);
    if (fileIndex !== 0) {
      const vector = row.slice(0, fileIndex).reverse();
      this.xRay.annotate(field, vector, Direction.Row);
    }
    if (fileIndex !== 7) {
      const vector = row.slice(fileIndex + 1);
      this.xRay.annotate(field, vector, Direction.Row);
    }
  }

  annotateFileAttack(field) {
    const rowIndex = field.row;
    const fileIndex = field.file;
    const vertical:Field[] = Pathfinder.getFile(this.board, fileIndex);

    if (rowIndex !== 0) {
      const vector = vertical.slice(0, rowIndex).reverse();
      this.xRay.annotate(field, vector, Direction.File);
    }
    if (rowIndex !== 7) {
      const vector = vertical.slice(rowIndex + 1);
      this.xRay.annotate(field, vector, Direction.File);
    }
  }

  annotateKingAttacks(field) {
    // TODO: yeah, right
    return field;
  }

  annotateTactics(field) {
    // TODO: Pins, cannons, forks...
    return field;
  }

  removePinnedAttacks({ field }) {
    if (!field.tactics.absolutePin) {
      return field;
    }

    field.attacks.forEach((attack) => {
      const target = this.board[attack.row][attack.file];
      target.attackers = target.attackers.filter(attacker => (
        attacker.piece !== field.piece
      ));
    });

    field.attacks = [];

    // maybe... remove all attacks and reapply them if needed ?


    // console.log(field.piece.type, field.tactics.absolutePin)


    return field;
  }

}
