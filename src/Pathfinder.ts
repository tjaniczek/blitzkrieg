export default class Pathfinder {
  static forEveryField(board, callback) {
    return board.map((row, rowIndex) => (
      row.map((field, fileIndex) => (
        callback({ field, rowIndex, fileIndex, row, board })
      ))
    ));
  }

  static getRow(board, targetRowIndex) {
    return board[targetRowIndex];
  }

  static getFile(board, targetFileIndex) {
    return board.map(row => row[targetFileIndex]);
  }

  static getRightDiagonal(board, targetRowIndex, targetFileIndex) {
    // TODO: may be faster to count all diagonals upfront and then get
    // one correspoding to the given position, but for now seems like
    // premature optimalisation. Using 0x88 is also way premature.
    const target = targetRowIndex + targetFileIndex;
    return board.map((row, index) => row[target - index])
      .filter(Pathfinder.filterInvalidMoves);
  }

  static getLeftDiagonal(board, targetRowIndex, targetFileIndex) {
    // TODO: see getRightDiagonal();
    const target = targetRowIndex - targetFileIndex;
    return board.map((row, index) => row[index - target])
      .filter(Pathfinder.filterInvalidMoves);
  }

  static getKnightMoves(board, targetRowIndex, targetFileIndex) {
    // TODO: may be better to use Chess.js .moves();
    const targets = [
      [targetRowIndex - 2, targetFileIndex - 1],
      [targetRowIndex - 2, targetFileIndex + 1],
      [targetRowIndex + 2, targetFileIndex - 1],
      [targetRowIndex + 2, targetFileIndex + 1],
      [targetRowIndex - 1, targetFileIndex - 2],
      [targetRowIndex + 1, targetFileIndex - 2],
      [targetRowIndex - 1, targetFileIndex + 2],
      [targetRowIndex + 1, targetFileIndex + 2],
    ];

    return targets.filter(Pathfinder.filterInvalidKnightMoves)
      .map(target => board[target[0]][target[1]]);
  }

  static filterInvalidMoves(move) {
    return move;
  }

  static filterInvalidKnightMoves(coordinates) {
    return coordinates[0] >= 0 &&
           coordinates[0] <= 7 &&
           coordinates[1] >= 0 &&
           coordinates[1] <= 7;
  }
}
