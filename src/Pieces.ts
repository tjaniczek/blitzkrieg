import { PieceInfo } from './types';

enum Piece { Pawn, Knight, Bishop, Rook, Queen, King };
enum Direction { RightDiagonal, LeftDiagonal, File, Row }

const Pieces:Array<PieceInfo> = [
  {
    id: Piece.Pawn,
    label: 'pawn',
    value: 1,
  },
  {
    id: Piece.Knight,
    label: 'knight',
    value: 3,
  },
  {
    id: Piece.Bishop,
    label: 'bishop',
    value: 3,
    diagonal: true,
  },
  {
    id: Piece.Rook,
    label: 'rook',
    value: 5,
    straight: true,
  },
  {
    id: Piece.Queen,
    label: 'queen',
    value: 9,
    diagonal: true,
    straight: true,
  },
  {
    id: Piece.King,
    label: 'king',
    value: 30, // TODO: come up with sensible value
  }
];

export {
  Piece,
  Pieces,
  Direction,
};
