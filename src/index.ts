import Coach from './Coach';

console.log('Compiled')
const fen = '2kr1b1r/ppp1ppp1/7p/3P1q2/2P5/1P2BN2/P1n2PPP/R2QR1K1 w - - 0 1';

console.time('annotate');
const coach = new Coach(fen);
coach.annotate();
console.timeEnd('annotate');


const html = document.querySelectorAll('pre')[0];
if (html) {
  html.innerHTML += coach.getAscii();
}
