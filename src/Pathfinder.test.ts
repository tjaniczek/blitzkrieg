import Pathfinder from './Pathfinder';

describe('Pathfinder', () => {
  let board;

  beforeEach(() => {
    board = [
      [ 1,  2,  3,  4,  5,  6,  7,  8],
      [ 9, 10, 11, 12, 13, 14, 15, 16],
      [17, 18, 19, 20, 21, 22, 23, 24],
      [25, 26, 27, 28, 29, 30, 31, 32],
      [33, 34, 35, 36, 37, 38, 39, 40],
      [41, 42, 43, 44, 45, 46, 47, 48],
      [49, 50, 51, 52, 53, 54, 55, 56],
      [57, 58, 59, 60, 61, 62, 63, 64],
    ];
  });

  it('can loop through each field on a chessboard', () => {
    const zero = () => 0;
    const emptyBoard = Pathfinder.forEveryField(board, zero);
    expect(emptyBoard[0][0]).toBe(0);

    const sumOfAllFields = emptyBoard.reduce((previousRow, row) => (
      previousRow + row.reduce((previousField, field) =>
        (previousField + field), 0)), 0);

    expect(sumOfAllFields).toBe(0);
  });

  it('loops through each field from top right to bottom left', () => {
    let i = 0;
    const nextNumber = () => i++;
    const emptyBoard = Pathfinder.forEveryField(board, nextNumber);
    expect(emptyBoard[0][0]).toBe(0);
    expect(emptyBoard[0][1]).toBe(1);
    expect(emptyBoard[0][2]).toBe(2);
    expect(emptyBoard[1][0]).toBe(8);
    expect(emptyBoard[7][7]).toBe(63);
  });

  it('can find zero-indexed row on a chessboard', () => {
    const row1 = Pathfinder.getRow(board, 0);
    const row2 = Pathfinder.getRow(board, 6);
    expect(row1).toEqual([1, 2, 3, 4, 5, 6, 7, 8]);
    expect(row2).toEqual([49, 50, 51, 52, 53, 54, 55, 56]);
  });

  it('can find zero-indexed file (column) on a chessboard', () => {
    const col1 = Pathfinder.getFile(board, 0);
    const col2 = Pathfinder.getFile(board, 3);
    expect(col1).toEqual([1, 9, 17, 25, 33, 41, 49, 57]);
    expect(col2).toEqual([4, 12, 20, 28, 36, 44, 52, 60]);
  });

  it('can find diagonal towards top right on a chessboard', () => {
    const diag1 = Pathfinder.getRightDiagonal(board, 0, 0);
    const diag2 = Pathfinder.getRightDiagonal(board, 3, 0);
    const diag3 = Pathfinder.getRightDiagonal(board, 4, 4);

    expect(diag1).toEqual([1]);
    expect(diag2).toEqual([4, 11, 18, 25]);
    expect(diag3).toEqual([16, 23, 30, 37, 44, 51, 58]);
  });

  it('can find diagonal towards top left on a chessboard', () => {
    const diag1 = Pathfinder.getLeftDiagonal(board, 0, 7);
    const diag2 = Pathfinder.getLeftDiagonal(board, 6, 2);
    const diag3 = Pathfinder.getLeftDiagonal(board, 4, 4);

    expect(diag1).toEqual([8]);
    expect(diag2).toEqual([33, 42, 51, 60]);
    expect(diag3).toEqual([1, 10, 19, 28, 37, 46, 55, 64]);
  });


  it('can find all possible Knight moves on a chessboard', () => {
    const knight1 = Pathfinder.getKnightMoves(board, 4, 3);
    const knight2 = Pathfinder.getKnightMoves(board, 1, 0);
    const knight3 = Pathfinder.getKnightMoves(board, 5, 6);

    expect(knight1.sort()).toEqual([19, 21, 26, 30, 42, 46, 51, 53]);
    expect(knight2.sort()).toEqual([19, 26, 3]);
    expect(knight3.sort()).toEqual([30, 32, 37, 53, 62, 64]);
  });
});
