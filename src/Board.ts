import { Chess } from 'chess.js';
import {
  PieceInfo,
  Field
} from './types';
import { Piece, Pieces } from './Pieces';
import Pathfinder from './Pathfinder';
import { Coordinates } from './Coordinates';

export default class Board {
  public chessJs: any;
  public board: Field[][];

  constructor(fenNotation?:string) {
    this.chessJs = Chess(fenNotation);
    this.board = this.create(this.chessJs.board());
  }

  getBoard() {
    return this.board;
  }

  getAscii() {
    return this.chessJs.ascii();
  }

  create(board) {
    const updateInfo = fieldInfo => this.updateField(fieldInfo);
    return Pathfinder.forEveryField(board, updateInfo);
  }

  updateField({ rowIndex: row, fileIndex: file, field: chessJsField }) {
    const field:Field = {
      ...Coordinates[row][file],
      attackers: [],
      defenders: [],
      attacks: [],
      defends: [],
      tactics: {
        pins: [],
        skewers: [],
        cannons: [],
        batteries: [],
        absolutePin: false,
        enPassant: false,
        enPassantBlocked: false,
      },
    };

    if (chessJsField) {
      field.piece = this.createPieceInfo(chessJsField);
    }

    return field;
  }

  createPieceInfo(chessJsField) {
    return {
      ...chessJsField,
      ...this.createPieceEvaluation(chessJsField.type),
    };
  }

  createPieceEvaluation(piece) {
    const tag = piece.toLowerCase();
    switch (tag) {
    case 'p':
      return Pieces[Piece.Pawn];
    case 'n':
      return Pieces[Piece.Knight];
    case 'b':
      return Pieces[Piece.Bishop];
    case 'r':
      return Pieces[Piece.Rook];
    case 'q':
      return Pieces[Piece.Queen];
    case 'k':
      return Pieces[Piece.King];
    default:
      return {};
    }
  }
}
