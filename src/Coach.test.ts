import CoachTester from './CoachTester';

describe('Coach', () => {
  const blackKing = {
    color: 'b',
    type: 'k',
  };

  const whiteRook = {
    color: 'w',
    type: 'r',
  };

  let coach;

  beforeEach(() => {
    //   +------------------------+
    // 8 | .  .  k  r  .  b  .  r |
    // 7 | p  p  p  .  p  p  p  . |
    // 6 | .  .  .  .  .  .  .  p |
    // 5 | .  .  .  P  .  q  .  . |
    // 4 | .  .  P  .  .  .  .  . |
    // 3 | .  P  .  .  B  N  .  . |
    // 2 | P  .  n  .  .  P  P  P |
    // 1 | R  .  .  Q  R  .  K  . |
    //   +------------------------+
    //     a  b  c  d  e  f  g  h

    const fen = '2kr1b1r/ppp1ppp1/7p/3P1q2/2P5/1P2BN2/P1n2PPP/R2QR1K1 w - - 0 1';
    coach = new CoachTester(fen);
    coach.annotate();
  });

  test('should create board with FEN notation', () => {
    expect(coach.pieceOn('a8')).toBeUndefined();
    expect(coach.pieceOn('c8')).toMatchObject(blackKing);
    expect(coach.pieceOn('e1')).toMatchObject(whiteRook);
  });
});
