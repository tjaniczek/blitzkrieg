import Board from './Board';

describe('Board', () => {

  //   +------------------------+
  // 8 | .  .  k  r  .  b  .  r |
  // 7 | p  p  p  .  p  p  p  . |
  // 6 | .  .  .  .  .  .  .  p |
  // 5 | .  .  .  P  .  q  .  . |
  // 4 | .  .  P  .  .  .  .  . |
  // 3 | .  P  .  .  B  N  .  . |
  // 2 | P  .  n  .  .  P  P  P |
  // 1 | R  .  .  Q  R  .  K  . |
  //   +------------------------+
  //     a  b  c  d  e  f  g  h

  const fen = '2kr1b1r/ppp1ppp1/7p/3P1q2/2P5/1P2BN2/P1n2PPP/R2QR1K1 w - - 0 1';

  const whiteRook = {
    color: 'w',
    id: 3,
    label: 'rook',
    type: 'r',
    value: 5,
  };

  const blackRook = {
    color: 'b',
    id: 3,
    label: 'rook',
    type: 'r',
    value: 5,
  };

  const blackKnight = {
    color: 'b',
    id: 1,
    label: 'knight',
    type: 'n',
    value: 3,
  };

  const whiteKing = {
    color: 'w',
    id: 5,
    label: 'king',
    type: 'k',
    value: 30,
  };

  const blackKing = {
    color: 'b',
    id: 5,
    label: 'king',
    type: 'k',
    value: 30,
  };

  test('should annotate board with array coordinates starting from a8', () => {
    let instance = new Board();
    let board = instance.getBoard();
    expect(board[0][0]).toMatchObject({
      file: 0,
      row: 0,
      label: 'a8',
    });
    expect(board[0][1]).toMatchObject({
      file: 1,
      row: 0,
      label: 'b8',
    });
    expect(board[7][0]).toMatchObject({
      file: 0,
      row: 7,
      label: 'a1',
    });
  });

  test('should annotate board with pieces starting from a8', () => {
    let instance = new Board();
    let board = instance.getBoard();
    expect(board[0][0].piece).toMatchObject(blackRook);
    expect(board[0][1].piece).toMatchObject(blackKnight);
    expect(board[7][4].piece).toMatchObject(whiteKing);
    expect(board[2][0].piece).toBeUndefined();
  });

  test('should create board with FEN notation', () => {
    let instance = new Board(fen);
    let board = instance.getBoard();
    expect(board[0][0].piece).toBeUndefined();
    expect(board[0][2].piece).toMatchObject(blackKing);
    expect(board[7][4].piece).toMatchObject(whiteRook);

  });
});
