import CoachTester from '../CoachTester';

describe('Coach on: Batteries', () => {
  test('should annotate pawn batteries', () => {
    //    +------------------------+
    //  8 | .  .  .  .  k  .  .  . |
    //  7 | .  .  .  r  .  p  .  . |
    //  6 | .  .  .  .  P  .  .  . |
    //  5 | .  .  .  .  .  B  .  . |
    //  4 | .  .  .  .  .  .  .  . |
    //  3 | .  Q  .  .  .  .  .  . |
    //  2 | .  .  .  .  .  .  .  . |
    //  1 | .  .  .  .  K  .  .  . |
    //    +------------------------+
    //      a  b  c  d  e  f  g  h

    const fen = '4k3/3r1p2/4P3/5B2/8/1Q6/8/4K3 w - - 0 1';
    const coach = new CoachTester(fen);
    coach.annotate();

    expect(coach.fieldOn('e6').tactics.batteries).toContainEqual({
      attacker: coach.relationTo('b3'),
      target: coach.relationTo('f7'),
    });

    expect(coach.fieldOn('e6').tactics.batteries).toContainEqual({
      attacker: coach.relationTo('f5'),
      target: coach.relationTo('d7'),
    });
  });

  test('should not annotate pawn battery if pawn is too far', () => {
    //    +------------------------+
    //  8 | .  .  .  .  k  .  .  . |
    //  7 | .  .  .  .  .  p  .  . |
    //  6 | .  .  .  .  .  .  .  . |
    //  5 | .  .  .  P  .  .  .  . |
    //  4 | .  .  .  .  .  .  .  . |
    //  3 | .  Q  .  .  .  .  .  . |
    //  2 | .  .  .  .  .  .  .  . |
    //  1 | .  .  .  .  K  .  .  . |
    //    +------------------------+
    //      a  b  c  d  e  f  g  h

    const fen = '4k3/5p2/8/3P4/8/1Q6/8/4K3 w - - 0 1';
    const coach = new CoachTester(fen);
    coach.annotate();

    expect(coach.fieldOn('d5').tactics.batteries).not.toContainEqual({
      attacker: coach.relationTo('b3'),
      target: coach.relationTo('f7'),
    });
  });

  test('should not annotate pawn battery if pawn is first', () => {
    //    +------------------------+
    //  8 | .  .  .  .  k  .  .  . |
    //  7 | .  .  .  .  .  p  .  . |
    //  6 | .  .  .  .  Q  .  .  . |
    //  5 | .  .  .  .  .  .  .  . |
    //  4 | .  .  .  .  .  .  .  . |
    //  3 | .  P  .  .  .  .  .  . |
    //  2 | .  .  .  .  .  .  .  . |
    //  1 | .  .  .  .  K  .  .  . |
    //    +------------------------+
    //      a  b  c  d  e  f  g  h

    const fen = '4k3/5p2/4Q3/8/8/1P6/8/4K3 w - - 0 1';
    const coach = new CoachTester(fen);
    coach.annotate();

    expect(coach.fieldOn('e6').tactics.batteries).not.toContainEqual({
      attacker: coach.relationTo('b3'),
      target: coach.relationTo('f7'),
    });
  });

  test('should not annotate pawn battery if pawn attacks en passant', () => {
    //    +------------------------+
    //  8 | .  .  .  .  k  .  .  . |
    //  7 | .  .  .  .  .  .  .  . |
    //  6 | .  .  .  .  .  .  .  . |
    //  5 | .  .  .  P  p  .  .  . |
    //  4 | .  .  .  .  .  .  .  . |
    //  3 | .  Q  .  .  .  .  .  . |
    //  2 | .  .  .  .  .  .  .  . |
    //  1 | .  .  .  .  K  .  .  . |
    //    +------------------------+
    //      a  b  c  d  e  f  g  h

    const fen = '4k3/8/8/3Pp3/8/1Q6/8/4K3 w - e6 0 1';
    const coach = new CoachTester(fen);
    coach.annotate();

    expect(coach.fieldOn('d5').tactics.batteries).not.toContainEqual({
      attacker: coach.relationTo('b3'),
      target: coach.relationTo('e6'),
    });
  });
});
