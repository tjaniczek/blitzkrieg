import CoachTester from '../CoachTester';

describe('Coach on: Skewers', () => {
  describe('using bishop', () => {
    let coach;
    beforeEach(() => {
      //    +------------------------+
      //  8 | .  .  .  k  .  .  .  . |
      //  7 | .  .  .  .  .  .  .  p |
      //  6 | n  .  .  .  .  .  p  . |
      //  5 | .  .  .  .  .  r  .  . |
      //  4 | .  .  q  .  .  .  .  . |
      //  3 | .  .  .  B  .  .  .  . |
      //  2 | .  .  .  .  .  .  .  . |
      //  1 | .  .  K  .  .  .  .  . |
      //    +------------------------+
      //      a  b  c  d  e  f  g  h

      const fen = '3k4/7p/n5p1/5r2/2q5/3B4/8/2K5 w - - 0 1';
      coach = new CoachTester(fen);
      coach.annotate();
    });

    test('should annotate pieces with skewers by bishop', () => {
      expect(coach.fieldOn('c4').tactics.skewers).toContainEqual({
        attacker: coach.relationTo('d3'),
        target: coach.relationTo('a6'),
      });

      expect(coach.fieldOn('f5').tactics.skewers).toContainEqual({
        attacker: coach.relationTo('d3'),
        target: coach.relationTo('g6'),
      });
    });

    test('should pin only one piece', () => {
      expect(coach.fieldOn('f5').tactics.skewers).not.toContainEqual({
        attacker: coach.relationTo('d3'),
        target: coach.relationTo('h7'),
      });
    });
  });

  describe('using queen on diagonal', () => {
    let coach;
    beforeEach(() => {
      //    +------------------------+
      //  8 | .  .  .  k  .  .  .  . |
      //  7 | .  .  .  .  .  .  .  p |
      //  6 | n  .  .  .  .  .  p  . |
      //  5 | .  .  .  .  .  r  .  . |
      //  4 | .  .  q  .  .  .  .  . |
      //  3 | .  .  .  Q  .  .  .  . |
      //  2 | .  .  .  .  .  .  .  . |
      //  1 | .  .  K  .  .  .  .  . |
      //    +------------------------+
      //      a  b  c  d  e  f  g  h

      const fen = '3k4/7p/n5p1/5r2/2q5/3Q4/8/2K5 w - - 0 1';
      coach = new CoachTester(fen);
      coach.annotate();
    });

    test('should annotate pieces with skewers by queen', () => {
      expect(coach.fieldOn('c4').tactics.skewers).toContainEqual({
        attacker: coach.relationTo('d3'),
        target: coach.relationTo('a6'),
      });

      expect(coach.fieldOn('f5').tactics.skewers).toContainEqual({
        attacker: coach.relationTo('d3'),
        target: coach.relationTo('g6'),
      });
    });

    test('should pin only one piece', () => {
      expect(coach.fieldOn('f5').tactics.skewers).not.toContainEqual({
        attacker: coach.relationTo('d3'),
        target: coach.relationTo('h7'),
      });
    });
  });

  describe('using rook', () => {
    let coach;
    beforeEach(() => {
      //    +------------------------+
      //  8 | .  .  .  .  .  .  .  k |
      //  7 | .  .  .  .  .  .  .  . |
      //  6 | .  .  .  .  .  .  .  . |
      //  5 | P  .  .  Q  r  R  N  R |
      //  4 | .  .  .  .  .  .  .  . |
      //  3 | .  .  .  .  .  .  .  . |
      //  2 | .  .  .  .  .  .  .  . |
      //  1 | K  .  .  .  .  .  .  . |
      //    +------------------------+
      //      a  b  c  d  e  f  g  h

      const fen = '7k/8/8/P2QrRNR/8/8/8/K7 w - - 0 1';
      coach = new CoachTester(fen);
      coach.annotate();
    });

    test('should annotate pieces with skewers by rook', () => {
      expect(coach.fieldOn('d5').tactics.skewers).toContainEqual({
        attacker: coach.relationTo('e5'),
        target: coach.relationTo('a5'),
      });

      expect(coach.fieldOn('f5').tactics.skewers).toContainEqual({
        attacker: coach.relationTo('e5'),
        target: coach.relationTo('g5'),
      });
    });

    test('should pin only one piece', () => {
      expect(coach.fieldOn('f5').tactics.skewers).not.toContainEqual({
        attacker: coach.relationTo('e5'),
        target: coach.relationTo('h5'),
      });
    });
  });

  describe('using queen on straight', () => {
    let coach;
    beforeEach(() => {
      //    +------------------------+
      //  8 | .  .  .  .  .  .  .  k |
      //  7 | .  .  .  .  .  .  .  . |
      //  6 | .  .  .  .  .  .  .  . |
      //  5 | P  .  .  Q  q  R  N  R |
      //  4 | .  .  .  .  .  .  .  . |
      //  3 | .  .  .  .  .  .  .  . |
      //  2 | .  .  .  .  .  .  .  . |
      //  1 | K  .  .  .  .  .  .  . |
      //    +------------------------+
      //      a  b  c  d  e  f  g  h

      const fen = '7k/8/8/P2QqRNR/8/8/8/K7 w - - 0 1';
      coach = new CoachTester(fen);
      coach.annotate();
    });

    test('should annotate pieces with skewers by queen', () => {
      expect(coach.fieldOn('d5').tactics.skewers).toContainEqual({
        attacker: coach.relationTo('e5'),
        target: coach.relationTo('a5'),
      });

      expect(coach.fieldOn('f5').tactics.skewers).toContainEqual({
        attacker: coach.relationTo('e5'),
        target: coach.relationTo('g5'),
      });
    });

    test('should pin only one piece', () => {
      expect(coach.fieldOn('f5').tactics.skewers).not.toContainEqual({
        attacker: coach.relationTo('e5'),
        target: coach.relationTo('h5'),
      });
    });
  });
});
