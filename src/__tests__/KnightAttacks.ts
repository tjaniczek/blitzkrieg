import CoachTester from '../CoachTester';

describe('Coach on: Knight attacks', () => {

  describe('for attacking white knight', () => {
    let coach;
    let knight;
    beforeEach(() => {
      //    +------------------------+
      //  8 | .  .  .  .  k  .  .  . |
      //  7 | .  .  .  .  .  .  .  . |
      //  6 | .  .  .  .  .  .  .  . |
      //  5 | .  .  p  .  .  .  .  . |
      //  4 | .  .  .  .  .  b  .  . |
      //  3 | .  .  .  N  .  .  .  . |
      //  2 | .  .  .  .  .  .  .  . |
      //  1 | .  .  .  .  .  K  .  . |
      //    +------------------------+
      //      a  b  c  d  e  f  g  h

      const fen = '4k3/8/8/2p5/5b2/3N4/8/5K2 w - - 0 1';
      coach = new CoachTester(fen);
      coach.annotate();
      knight = coach.fieldOn('d3');
    });

    test('should annotate white knight attaks', () => {
      expect(knight.attacks)
        .toContainEqual(coach.relationTo('c5'));
      expect(knight.attacks)
        .toContainEqual(coach.relationTo('b4'));
      expect(knight.attacks)
        .toContainEqual(coach.relationTo('b2'));
      expect(knight.attacks)
        .toContainEqual(coach.relationTo('c1'));
      expect(knight.attacks)
        .toContainEqual(coach.relationTo('e1'));
      expect(knight.attacks)
        .toContainEqual(coach.relationTo('f2'));
      expect(knight.attacks)
        .toContainEqual(coach.relationTo('f4'));
      expect(knight.attacks)
        .toContainEqual(coach.relationTo('e5'));

      expect(coach.fieldOn('c5').attackers)
        .toContainEqual(coach.relationTo('d3'));
      expect(coach.fieldOn('f4').attackers)
        .toContainEqual(coach.relationTo('d3'));
    });

  });

  describe('for defending white knight', () => {
    let coach;
    let knight;
    beforeEach(() => {
      //    +------------------------+
      //  8 | .  .  .  .  k  .  .  . |
      //  7 | .  .  .  .  .  .  .  . |
      //  6 | .  .  .  .  .  .  .  . |
      //  5 | .  .  P  .  .  .  .  . |
      //  4 | .  .  .  .  .  Q  .  . |
      //  3 | .  .  .  N  .  .  .  . |
      //  2 | .  .  .  .  .  R  .  . |
      //  1 | .  .  .  .  .  K  .  . |
      //    +------------------------+
      //      a  b  c  d  e  f  g  h

      const fen = '4k3/8/8/2P5/5Q2/3N4/5R2/5K2 w - - 0 1';
      coach = new CoachTester(fen);
      coach.annotate();
      knight = coach.fieldOn('d3');
    });

    test('should annotate white knight defences', () => {
      expect(knight.defends)
        .toContainEqual(coach.relationTo('c5'));
      expect(knight.defends)
        .toContainEqual(coach.relationTo('f4'));
      expect(knight.defends)
        .toContainEqual(coach.relationTo('f2'));

      expect(coach.fieldOn('f4').defenders)
        .toContainEqual(coach.relationTo('d3'));
      expect(coach.fieldOn('f2').defenders)
        .toContainEqual(coach.relationTo('d3'));
    });
  });

  describe('for black knight', () => {
    let coach;
    let knight;
    beforeEach(() => {
      //    +------------------------+
      //  8 | .  .  .  .  k  .  .  . |
      //  7 | .  .  .  .  .  .  .  . |
      //  6 | .  .  .  .  .  .  .  . |
      //  5 | .  .  .  .  .  b  .  . |
      //  4 | .  .  .  .  .  .  .  . |
      //  3 | .  .  .  .  n  .  .  . |
      //  2 | .  .  P  .  .  .  .  . |
      //  1 | .  .  .  .  .  K  .  . |
      //    +------------------------+
      //      a  b  c  d  e  f  g  h

      const fen = '4k3/8/8/5b2/8/4n3/2P5/5K2 w - - 0 1';
      coach = new CoachTester(fen);
      coach.annotate();
      knight = coach.fieldOn('e3');
    });

    test('should annotate black knight attacks', () => {
      expect(knight.attacks)
        .toContainEqual(coach.relationTo('c2'));
      expect(knight.attacks)
        .toContainEqual(coach.relationTo('f1'));
      expect(knight.attacks)
        .toContainEqual(coach.relationTo('g2'));

      expect(coach.fieldOn('c2').attackers)
        .toContainEqual(coach.relationTo('e3'));
      expect(coach.fieldOn('f1').attackers)
        .toContainEqual(coach.relationTo('e3'));
      expect(coach.fieldOn('g2').attackers)
        .toContainEqual(coach.relationTo('e3'));
    });

    test('should annotate black knight defences', () => {
      expect(knight.defends)
        .toContainEqual(coach.relationTo('f5'));

      expect(coach.fieldOn('f5').defenders)
        .toContainEqual(coach.relationTo('e3'));
    });
  });

  describe('for attacks from the edge', () => {
    let coach;
    let blackKnight;
    let whiteKnight;
    beforeEach(() => {
      //    +------------------------+
      //  8 | .  .  .  .  k  .  .  . |
      //  7 | .  .  .  .  .  .  .  . |
      //  6 | .  .  .  .  .  .  .  n |
      //  5 | .  .  .  .  .  .  .  . |
      //  4 | .  .  .  .  .  .  .  . |
      //  3 | .  .  .  .  .  .  .  . |
      //  2 | .  .  .  .  .  .  .  . |
      //  1 | N  .  .  .  .  K  .  . |
      //    +------------------------+
      //      a  b  c  d  e  f  g  h

      const fen = '4k3/8/7n/8/8/8/8/N4K2 w - - 0 1';
      coach = new CoachTester(fen);
      coach.annotate();
      blackKnight = coach.fieldOn('h6');
      whiteKnight = coach.fieldOn('a1');
    });

    test('should annotate only possible attacks', () => {
      expect(whiteKnight.attacks).toHaveLength(2);
      expect(blackKnight.attacks).toHaveLength(4);
    });
  });
});
