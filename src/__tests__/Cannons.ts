import CoachTester from '../CoachTester';

describe('Coach on: Cannons', () => {
  test('should annotate cannon backed by bishop', () => {
    //    +------------------------+
    //  8 | .  .  .  .  .  .  .  k |
    //  7 | .  b  .  .  .  .  q  . |
    //  6 | .  .  n  .  .  N  .  . |
    //  5 | .  .  .  .  .  .  .  . |
    //  4 | .  .  .  .  .  .  .  . |
    //  3 | .  .  .  .  .  .  .  . |
    //  2 | .  B  .  .  .  .  N  . |
    //  1 | .  K  .  .  .  .  .  . |
    //    +------------------------+
    //      a  b  c  d  e  f  g  h

    const fen = '7k/1b4q1/2n2N2/8/8/8/1B4N1/1K6 w - - 0 1';
    const coach = new CoachTester(fen);
    coach.annotate();

    expect(coach.fieldOn('f6').tactics.cannons).toContainEqual({
      attacker: coach.relationTo('b2'),
      target: coach.relationTo('g7'),
    });

    expect(coach.fieldOn('c6').tactics.cannons).toContainEqual({
      attacker: coach.relationTo('b7'),
      target: coach.relationTo('g2'),
    });
  });

  test('should annotate cannon if pawn can move', () => {
    //    +------------------------+
    //  8 | .  .  .  .  .  .  .  k |
    //  7 | .  .  .  .  .  .  .  . |
    //  6 | .  .  .  .  .  P  .  . |
    //  5 | .  .  .  .  .  .  .  . |
    //  4 | .  .  .  .  .  .  .  . |
    //  3 | .  .  .  .  .  .  .  . |
    //  2 | .  B  .  .  .  .  .  . |
    //  1 | .  .  .  .  K  .  .  . |
    //    +------------------------+
    //      a  b  c  d  e  f  g  h

    const fen = '7k/8/5P2/8/8/8/1B6/4K3 w - - 0 1';
    const coach = new CoachTester(fen);
    coach.annotate();

    expect(coach.fieldOn('f6').tactics.cannons).toContainEqual({
      attacker: coach.relationTo('b2'),
      target: coach.relationTo('h8'),
    });
  });

  test('should not annotate cannon if pawn has no moves', () => {
    //    +------------------------+
    //  8 | .  .  .  .  .  .  .  k |
    //  7 | .  .  .  .  .  p  .  . |
    //  6 | .  .  .  .  .  P  .  . |
    //  5 | .  .  .  .  .  .  .  . |
    //  4 | .  .  .  .  .  .  .  . |
    //  3 | .  .  .  .  .  .  .  . |
    //  2 | .  B  .  .  .  .  .  . |
    //  1 | .  .  .  .  K  .  .  . |
    //    +------------------------+
    //      a  b  c  d  e  f  g  h

    const fen = '7k/5p2/5P2/8/8/8/1B6/4K3 w - - 0 1';
    const coach = new CoachTester(fen);
    coach.annotate();

    expect(coach.fieldOn('f6').tactics.cannons).not.toContainEqual({
      attacker: coach.relationTo('b2'),
      target: coach.relationTo('h8'),
    });
  });

  test('should annotate cannon backed by rook', () => {
    //    +------------------------+
    //  8 | .  .  .  .  k  .  .  . |
    //  7 | .  .  .  .  .  .  .  . |
    //  6 | .  .  .  .  .  .  .  . |
    //  5 | .  .  .  .  .  .  .  . |
    //  4 | .  .  .  .  .  .  .  . |
    //  3 | .  .  .  .  .  .  .  . |
    //  2 | .  .  .  .  B  .  .  . |
    //  1 | .  .  .  .  R  K  .  . |
    //    +------------------------+
    //      a  b  c  d  e  f  g  h

    const fen = '4k3/8/8/8/8/8/4B3/4RK2 w - - 0 1';
    const coach = new CoachTester(fen);
    coach.annotate();

    expect(coach.fieldOn('e2').tactics.cannons).toContainEqual({
      attacker: coach.relationTo('e1'),
      target: coach.relationTo('e8'),
    });
  });

  test('should annotate cannon on straight', () => {
    //    +------------------------+
    //  8 | .  .  .  .  .  .  .  . |
    //  7 | .  .  .  .  .  .  .  . |
    //  6 | .  .  .  .  .  .  .  . |
    //  5 | .  k  .  .  .  .  .  . |
    //  4 | .  .  .  .  .  .  .  . |
    //  3 | .  .  .  .  .  .  .  . |
    //  2 | .  .  .  .  B  .  .  . |
    //  1 | .  .  .  .  R  K  .  . |
    //    +------------------------+
    //      a  b  c  d  e  f  g  h

    const fen = '8/8/8/1k6/8/8/4B3/4RK2 w - - 0 1';
    const coach = new CoachTester(fen);
    coach.annotate();

    expect(coach.fieldOn('e2').tactics.cannons).not.toContainEqual({
      attacker: coach.relationTo('e1'),
      target: coach.relationTo('b5'),
    });
  });

  test('should annotate rook cannon on pawn only if pawn has target', () => {
    //    +------------------------+
    //  8 | .  .  .  .  k  .  .  . |
    //  7 | .  p  .  .  .  .  .  . |
    //  6 | .  .  p  .  .  .  .  . |
    //  5 | .  P  .  .  .  .  .  . |
    //  4 | .  .  .  .  .  .  .  . |
    //  3 | .  .  .  .  .  .  .  . |
    //  2 | .  .  .  .  P  .  .  . |
    //  1 | .  R  .  .  R  K  .  . |
    //    +------------------------+
    //      a  b  c  d  e  f  g  h

    const fen = '4k3/1p6/2p5/1P6/8/8/4P3/1R2RK2 w - - 0 1';
    const coach = new CoachTester(fen);
    coach.annotate();
    expect(coach.fieldOn('b5').tactics.cannons).toContainEqual({
      attacker: coach.relationTo('b1'),
      target: coach.relationTo('b7'),
    });
    expect(coach.fieldOn('e2').tactics.cannons).not.toContainEqual({
      attacker: coach.relationTo('e1'),
      target: coach.relationTo('e8'),
    });
  });
});
