import CoachTester from '../CoachTester';

describe.skip('Coach on: Double attakcs', () => {
  test('should annotate double attacks backed by bishop', () => {
    //    +------------------------+
    //  8 | .  .  .  k  .  .  .  . |
    //  7 | .  .  .  .  .  .  .  . |
    //  6 | .  .  .  .  .  .  .  . |
    //  5 | .  .  .  .  .  n  .  . |
    //  4 | .  .  .  .  Q  .  .  . |
    //  3 | .  .  .  B  .  .  .  . |
    //  2 | .  .  .  .  .  .  .  . |
    //  1 | .  .  .  .  .  .  .  K |
    //    +------------------------+
    //      a  b  c  d  e  f  g  h

    const fen = '3k4/8/8/5n2/4Q3/3B4/8/7K w - - 0 1';
    const coach = new CoachTester(fen);
    coach.annotate();

    expect(coach.fieldOn('f5').attackers).toHaveLength(2);
  });

  test('should annotate double attacks backed by rook', () => {
    //    +------------------------+
    //  8 | .  .  .  k  .  .  .  . |
    //  7 | .  .  .  .  p  .  .  . |
    //  6 | .  .  .  .  .  .  .  . |
    //  5 | .  .  .  .  .  .  .  . |
    //  4 | .  .  .  .  Q  .  .  . |
    //  3 | .  .  .  .  .  .  .  . |
    //  2 | .  .  .  .  .  .  .  . |
    //  1 | .  .  .  .  R  .  .  K |
    //    +------------------------+
    //      a  b  c  d  e  f  g  h

    const fen = '3k4/4p3/8/8/4Q3/8/8/4R2K w - - 0 1';
    const coach = new CoachTester(fen);
    coach.annotate();

    expect(coach.fieldOn('e7').attackers).toHaveLength(2);
  });

  test('should annotate double attacks backed by queen', () => {
    //    +------------------------+
    //  8 | .  .  .  k  .  .  .  . |
    //  7 | .  .  .  .  p  .  .  . |
    //  6 | .  .  .  .  .  .  .  . |
    //  5 | n  .  .  .  .  .  .  . |
    //  4 | .  .  .  .  R  .  .  . |
    //  3 | .  .  .  .  .  .  .  . |
    //  2 | .  .  .  B  .  .  .  . |
    //  1 | .  .  .  .  Q  .  .  K |
    //    +------------------------+
    //      a  b  c  d  e  f  g  h

    const fen = '3k4/4p3/8/n7/4R3/8/3B4/4Q2K w - - 0 1';
    const coach = new CoachTester(fen);
    coach.annotate();

    expect(coach.fieldOn('e7').attackers).toHaveLength(2);
    expect(coach.fieldOn('a5').attackers).toHaveLength(2);
  });

  test('should annotate double attacks with pawn', () => {
    //    +------------------------+
    //  8 | .  .  .  k  .  .  .  . |
    //  7 | .  .  .  .  .  .  .  . |
    //  6 | .  .  .  .  .  .  p  . |
    //  5 | .  .  .  .  .  P  .  . |
    //  4 | .  .  .  .  .  .  .  . |
    //  3 | .  .  .  .  .  .  .  . |
    //  2 | .  .  B  .  .  .  .  . |
    //  1 | .  .  .  .  .  .  .  K |
    //    +------------------------+
    //      a  b  c  d  e  f  g  h

    const fen = '3k4/8/6p1/5P2/8/8/2B5/7K w - - 0 1';
    const coach = new CoachTester(fen);
    coach.annotate();

    expect(coach.fieldOn('g6').attackers).toHaveLength(2);
  });

  test('should not annotate double attacks with pawn if pawn cant attack', () => {
    //    +------------------------+
    //  8 | .  .  .  k  .  .  .  . |
    //  7 | .  .  .  .  .  .  .  . |
    //  6 | .  .  .  .  .  .  p  . |
    //  5 | .  .  .  .  .  .  .  . |
    //  4 | .  .  .  .  P  .  .  . |
    //  3 | .  .  .  .  .  .  .  . |
    //  2 | .  .  B  .  .  .  .  . |
    //  1 | .  .  .  .  .  .  .  K |
    //    +------------------------+
    //      a  b  c  d  e  f  g  h

    const fen = '3k4/8/6p1/8/4P3/8/2B5/7K w - - 0 1';
    const coach = new CoachTester(fen);
    coach.annotate();

    expect(coach.fieldOn('g6').attackers).toHaveLength(0);
  });

  test('should not annotate tripple attacks on diagonal', () => {
    //    +------------------------+
    //  8 | .  .  .  k  .  .  .  . |
    //  7 | .  .  .  .  .  .  .  . |
    //  6 | .  .  .  .  .  .  .  . |
    //  5 | .  .  .  .  .  q  .  . |
    //  4 | .  .  .  .  P  .  .  . |
    //  3 | .  .  .  Q  .  .  .  . |
    //  2 | .  .  B  .  .  .  .  . |
    //  1 | .  .  .  .  .  .  .  K |
    //    +------------------------+
    //      a  b  c  d  e  f  g  h

    const fen = '3k4/8/8/5q2/4P3/3Q4/2B5/7K w - - 0 1';
    const coach = new CoachTester(fen);
    coach.annotate();

    expect(coach.fieldOn('f5').attackers).toHaveLength(3);
  });

  test('should not annotate tripple attacks on straight', () => {
    //    +------------------------+
    //  8 | .  .  k  r  .  .  .  . |
    //  7 | .  .  .  .  .  .  .  . |
    //  6 | .  .  .  .  .  .  .  . |
    //  5 | .  .  .  .  .  .  .  . |
    //  4 | .  .  .  .  .  .  .  . |
    //  3 | .  .  .  Q  .  .  .  . |
    //  2 | .  .  .  R  .  .  .  . |
    //  1 | .  .  .  R  .  .  .  K |
    //    +------------------------+
    //      a  b  c  d  e  f  g  h

    const fen = '2kr4/8/8/8/8/3Q4/3R4/3R3K w - - 0 1';
    const coach = new CoachTester(fen);
    coach.annotate();

    expect(coach.fieldOn('d8').attackers).toHaveLength(3);
  });

  test('double attacks with enpassant', () => {
    throw new Error('Because fuck you, that\'s why');
  });
});
