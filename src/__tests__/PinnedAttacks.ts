import CoachTester from '../CoachTester';

describe('Coach on: Attacks of pinned pieces', () => {
  describe('should not annotate attacks of an absolutely pinned piece', () => {
    test('on diagonal', () => {
      //    +------------------------+
      //  8 | .  .  .  k  .  .  .  . |
      //  7 | .  .  .  .  .  .  .  . |
      //  6 | .  .  .  .  P  r  .  . |
      //  5 | .  .  .  .  .  .  B  . |
      //  4 | .  .  .  .  .  .  .  . |
      //  3 | .  .  .  .  .  .  .  . |
      //  2 | .  .  .  .  .  P  .  . |
      //  1 | .  .  .  K  .  .  .  . |
      //    +------------------------+
      //      a  b  c  d  e  f  g  h

      const fen = '3k4/8/4Pr2/6B1/8/8/5P2/3K4 w - - 0 1';
      let coach = new CoachTester(fen);
      coach.annotate();
      expect(coach.fieldOn('f2').attackers).toHaveLength(0);
      expect(coach.fieldOn('e6').attackers).toHaveLength(0);
      expect(coach.fieldOn('f6').attacks).toHaveLength(0);
    });

    // TODO: now add on straight
    // then add cases when it's possible to move (moves along pinned vector)
    // then test knights
    // then pawns
    // then it's time to look for kings

  });
});
