import CoachTester from '../CoachTester';

describe('Coach on: Bishop attacks', () => {

  describe('for attacking white bishop', () => {
    let coach;
    let bishop;
    beforeEach(() => {
      //    +------------------------+
      //  8 | .  .  .  .  k  .  .  . |
      //  7 | p  .  .  .  .  .  r  . |
      //  6 | .  .  .  .  .  .  .  . |
      //  5 | .  .  .  .  .  .  .  . |
      //  4 | .  .  .  B  .  .  .  . |
      //  3 | .  .  .  .  n  .  .  . |
      //  2 | .  q  .  .  .  .  .  . |
      //  1 | .  .  .  .  K  .  .  . |
      //    +------------------------+
      //      a  b  c  d  e  f  g  h

      const fen = '4k3/p5r1/8/8/3B4/4n3/1q6/4K3 w - - 0 1';
      coach = new CoachTester(fen);
      coach.annotate();
      bishop = coach.fieldOn('d4');
    });

    test('should annotate white bishop attaks', () => {
      expect(bishop.attacks)
        .toContainEqual(coach.relationTo('b2'));
      expect(bishop.attacks)
        .toContainEqual(coach.relationTo('g7'));
      expect(bishop.attacks)
        .toContainEqual(coach.relationTo('e3'));
      expect(bishop.attacks)
        .toContainEqual(coach.relationTo('a7'));

      expect(coach.fieldOn('b2').attackers)
        .toContainEqual(coach.relationTo('d4'));
      expect(coach.fieldOn('g7').attackers)
        .toContainEqual(coach.relationTo('d4'));
      expect(coach.fieldOn('e3').attackers)
        .toContainEqual(coach.relationTo('d4'));
      expect(coach.fieldOn('a7').attackers)
        .toContainEqual(coach.relationTo('d4'));
    });

    test('should annotate possible attacks for white bishop', () => {
      expect(bishop.attacks)
        .toContainEqual(coach.relationTo('c5'));
      expect(bishop.attacks)
        .toContainEqual(coach.relationTo('b6'));
      expect(bishop.attacks)
        .toContainEqual(coach.relationTo('f6'));
    });

    test('should not annotate attacks that would pass through opponents figure', () => {
      const bishop = coach.fieldOn('d4');
      expect(bishop.attacks)
        .not.toContainEqual(coach.relationTo('a1'));
      expect(bishop.attacks)
        .not.toContainEqual(coach.relationTo('h8'));
      expect(bishop.attacks)
        .not.toContainEqual(coach.relationTo('f2'));
      expect(bishop.attacks)
        .not.toContainEqual(coach.relationTo('g1'));
    });
  });

  describe('for defending white bishop', () => {
    let coach;
    let bishop;
    beforeEach(() => {
      //    +------------------------+
      //  8 | .  .  .  .  k  .  .  . |
      //  7 | .  .  .  .  .  .  .  Q |
      //  6 | .  .  .  .  .  .  .  . |
      //  5 | .  .  .  N  .  .  .  . |
      //  4 | .  .  .  .  B  .  .  . |
      //  3 | .  .  .  .  .  .  .  . |
      //  2 | .  .  P  .  .  .  .  . |
      //  1 | .  .  .  .  K  .  .  R |
      //    +------------------------+
      //      a  b  c  d  e  f  g  h

      const fen = '4k3/7Q/8/3N4/4B3/8/2P5/4K2R w - - 0 1';
      coach = new CoachTester(fen);
      coach.annotate();
      bishop = coach.fieldOn('e4');
    });

    test('should annotate white bishop defences', () => {
      expect(bishop.defends)
        .toContainEqual(coach.relationTo('c2'));
      expect(bishop.defends)
        .toContainEqual(coach.relationTo('d5'));
      expect(bishop.defends)
        .toContainEqual(coach.relationTo('h1'));
      expect(bishop.defends)
        .toContainEqual(coach.relationTo('h7'));

      expect(coach.fieldOn('c2').defenders)
        .toContainEqual(coach.relationTo('e4'));
      expect(coach.fieldOn('d5').defenders)
        .toContainEqual(coach.relationTo('e4'));
      expect(coach.fieldOn('h1').defenders)
        .toContainEqual(coach.relationTo('e4'));
      expect(coach.fieldOn('h7').defenders)
        .toContainEqual(coach.relationTo('e4'));
    });
  });

  describe('for black bishop', () => {
    let coach;
    let bishop;
    beforeEach(() => {
      //  +------------------------+
      //  8 | .  .  .  k  .  .  .  . |
      //  7 | .  .  .  .  p  .  .  . |
      //  6 | .  n  .  .  .  .  .  . |
      //  5 | .  .  b  .  .  .  .  . |
      //  4 | .  P  .  .  .  .  .  . |
      //  3 | .  .  .  .  .  .  .  . |
      //  2 | .  .  .  .  .  .  .  . |
      //  1 | .  .  .  .  .  .  K  . |
      //    +------------------------+
      //      a  b  c  d  e  f  g  h

      const fen = '3k4/4p3/1n6/2b5/1P6/8/8/6K1 w - - 0 1';
      coach = new CoachTester(fen);
      coach.annotate();
      bishop = coach.fieldOn('c5');
    });

    test('should black white bishop attacks', () => {
      expect(bishop.attacks)
      .toContainEqual(coach.relationTo('g1'));
      expect(bishop.attacks)
      .toContainEqual(coach.relationTo('b4'));
      expect(coach.fieldOn('g1').attackers)
      .toContainEqual(coach.relationTo('c5'));
      expect(coach.fieldOn('b4').attackers)
      .toContainEqual(coach.relationTo('c5'));
    });

    test('should annotate possible black bishop attacks', () => {
      expect(bishop.attacks)
        .toContainEqual(coach.relationTo('e3'));
      expect(bishop.attacks)
        .not.toContainEqual(coach.relationTo('a3'));
      expect(bishop.attacks)
        .not.toContainEqual(coach.relationTo('f8'));
    });

    test('should annotate black bishop defences', () => {
      expect(bishop.defends)
        .toContainEqual(coach.relationTo('b6'));
      expect(bishop.defends)
        .toContainEqual(coach.relationTo('e7'));
      expect(coach.fieldOn('b6').defenders)
        .toContainEqual(coach.relationTo('c5'));
      expect(coach.fieldOn('e7').defenders)
        .toContainEqual(coach.relationTo('c5'));
    });
  });

  describe('for attacks from the edge', () => {
    let coach;
    let blackBishop;
    let whiteBishop;
    beforeEach(() => {
      //   +------------------------+
      // 8 | .  .  .  .  .  .  .  k |
      // 7 | .  .  .  .  .  .  p  . |
      // 6 | .  .  .  .  .  .  .  . |
      // 5 | .  .  .  .  .  .  n  . |
      // 4 | .  .  .  .  .  .  .  b |
      // 3 | .  .  .  .  .  .  .  . |
      // 2 | .  .  .  .  .  .  .  . |
      // 1 | B  .  .  .  K  .  .  . |
      //   +------------------------+
      //     a  b  c  d  e  f  g  h

      const fen = '7k/6p1/8/6n1/7b/8/8/B3K3 w - - 0 1';
      coach = new CoachTester(fen);
      coach.annotate();
      blackBishop = coach.fieldOn('h4');
      whiteBishop = coach.fieldOn('a1');
    });

    test('should annotate all possible attacks and defences', () => {
      expect(blackBishop.defends)
        .toContainEqual(coach.relationTo('g5'));

      expect(blackBishop.attacks)
        .toContainEqual(coach.relationTo('g3'));
      expect(blackBishop.attacks)
        .toContainEqual(coach.relationTo('e1'));
      expect(coach.fieldOn('e1').attackers)
        .toContainEqual(coach.relationTo('h4'));

      expect(whiteBishop.attacks)
        .toContainEqual(coach.relationTo('b2'));
      expect(whiteBishop.attacks)
        .toContainEqual(coach.relationTo('g7'));
      expect(coach.fieldOn('g7').attackers)
        .toContainEqual(coach.relationTo('a1'));

      expect(whiteBishop.attacks)
        .not.toContainEqual(coach.relationTo('h8'));
    });

    test('should annotate only possible attacks', () => {
      expect(whiteBishop.attacks).toHaveLength(6);
      expect(blackBishop.attacks).toHaveLength(3);
      expect(blackBishop.defends).toHaveLength(1);
    });
  });
});
