import CoachTester from '../CoachTester';

describe('Coach on: Batteries', () => {
  test('should annotate battery backed by bishop', () => {
    //    +------------------------+
    //  8 | .  b  .  k  .  .  .  . |
    //  7 | .  .  q  .  .  .  .  . |
    //  6 | .  .  .  .  .  .  .  . |
    //  5 | .  .  .  .  .  n  .  . |
    //  4 | .  .  .  .  P  .  .  . |
    //  3 | .  .  .  B  .  .  .  . |
    //  2 | .  .  .  .  .  .  .  P |
    //  1 | .  .  .  .  .  .  .  K |
    //    +------------------------+
    //      a  b  c  d  e  f  g  h

    const fen = '1b1k4/2q5/8/5n2/4P3/3B4/7P/7K w - - 0 1';
    const coach = new CoachTester(fen);
    coach.annotate();

    expect(coach.fieldOn('e4').tactics.batteries).toContainEqual({
      attacker: coach.relationTo('d3'),
      target: coach.relationTo('f5'),
    });

    expect(coach.fieldOn('c7').tactics.batteries).toContainEqual({
      attacker: coach.relationTo('b8'),
      target: coach.relationTo('h2'),
    });
  });

  test('should annotate battery backed by queen on diagonal', () => {
    //    +------------------------+
    //  8 | .  q  .  k  .  .  .  . |
    //  7 | .  .  b  .  .  .  .  . |
    //  6 | .  .  .  .  .  .  .  . |
    //  5 | .  .  .  .  .  n  .  . |
    //  4 | .  .  .  .  Q  .  .  . |
    //  3 | .  .  .  B  .  .  .  . |
    //  2 | .  .  .  .  .  .  .  P |
    //  1 | .  .  .  .  .  .  .  K |
    //    +------------------------+
    //      a  b  c  d  e  f  g  h

    const fen = '1q1k4/2b5/8/5n2/4Q3/3B4/7P/7K w - - 0 1';
    const coach = new CoachTester(fen);
    coach.annotate();

    expect(coach.fieldOn('e4').tactics.batteries).toContainEqual({
      attacker: coach.relationTo('d3'),
      target: coach.relationTo('f5'),
    });

    expect(coach.fieldOn('c7').tactics.batteries).toContainEqual({
      attacker: coach.relationTo('b8'),
      target: coach.relationTo('h2'),
    });
  });

  test('should annotate battery backed by rook', () => {
    //    +------------------------+
    //  8 | .  .  .  .  k  .  .  . |
    //  7 | .  .  .  .  .  .  .  . |
    //  6 | .  .  .  .  .  .  .  . |
    //  5 | .  .  .  .  .  .  .  . |
    //  4 | .  .  .  .  .  .  .  . |
    //  3 | .  .  .  .  .  .  .  . |
    //  2 | .  .  .  .  Q  .  .  . |
    //  1 | .  .  .  .  R  K  .  . |
    //    +------------------------+
    //      a  b  c  d  e  f  g  h

    const fen = '4k3/8/8/8/8/8/4Q3/4RK2 w - - 0 1';
    const coach = new CoachTester(fen);
    coach.annotate();

    expect(coach.fieldOn('e2').tactics.batteries).toContainEqual({
      attacker: coach.relationTo('e1'),
      target: coach.relationTo('e8'),
    });
  });

  test('should annotate battery backed by queen on straight', () => {
    //    +------------------------+
    //  8 | .  .  .  .  k  .  .  . |
    //  7 | .  .  .  .  .  .  .  . |
    //  6 | .  .  .  .  .  .  .  . |
    //  5 | .  .  .  .  .  .  .  . |
    //  4 | .  .  .  .  .  .  .  . |
    //  3 | .  .  .  .  .  .  .  . |
    //  2 | .  .  .  .  R  .  .  . |
    //  1 | .  .  .  .  Q  K  .  . |
    //    +------------------------+
    //      a  b  c  d  e  f  g  h

    const fen = '4k3/8/8/8/8/8/4R3/4QK2 w - - 0 1';
    const coach = new CoachTester(fen);
    coach.annotate();

    expect(coach.fieldOn('e2').tactics.batteries).toContainEqual({
      attacker: coach.relationTo('e1'),
      target: coach.relationTo('e8'),
    });
  });
});
