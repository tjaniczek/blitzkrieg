import CoachTester from '../CoachTester';

describe('Coach on: Valid En Passant from FEN notation', () => {

  const pieceOn = (board, row, file) => ({
    row, file, piece: board[row][file].piece
  });

  test('should annotate black pawn attack', () => {
    //    +------------------------+
    //  8 | .  .  .  k  .  .  .  . |
    //  7 | .  .  .  .  .  .  .  . |
    //  6 | .  .  .  .  .  .  .  . |
    //  5 | .  .  .  .  .  .  .  . |
    //  4 | .  .  .  P  p  .  .  . |
    //  3 | .  .  .  .  .  .  .  . |
    //  2 | .  .  .  .  .  .  .  . |
    //  1 | .  .  .  K  .  .  .  . |
    //    +------------------------+
    //      a  b  c  d  e  f  g  h

    const fen = '3k4/8/8/8/3Pp3/8/8/3K4 b - d3 0 1';
    const coach = new CoachTester(fen);
    coach.annotate();

    expect(coach.fieldOn('d4').attackers)
      .toContainEqual(coach.relationTo('e4'));

    expect(coach.fieldOn('e4').attacks)
      .toContainEqual(coach.relationTo('d4'));

    expect(coach.fieldOn('d4').attackers[0].piece.color)
      .toBe('b');
  });


  test('should annotate white pawn attacks', () => {
    //    +------------------------+
    //  8 | .  .  .  k  .  .  .  . |
    //  7 | .  .  .  .  .  .  .  . |
    //  6 | .  .  .  .  .  .  .  . |
    //  5 | .  .  .  .  P  p  .  . |
    //  4 | .  .  .  .  .  .  .  . |
    //  3 | .  .  .  .  .  .  .  . |
    //  2 | .  .  .  .  .  .  .  . |
    //  1 | .  .  .  K  .  .  .  . |
    //    +------------------------+
    //      a  b  c  d  e  f  g  h

    const fen = '3k4/8/8/4Pp2/8/8/8/3K4 w - f6 0 1';
    const coach = new CoachTester(fen);
    coach.annotate();

    expect(coach.fieldOn('f5').attackers)
      .toContainEqual(coach.relationTo('e5'));
    expect(coach.fieldOn('e5').attacks)
      .toContainEqual(coach.relationTo('f5'));

    expect(coach.fieldOn('f5').attackers[0].piece.color)
      .toBe('w');
  });

  test('should annotate double en passan possibilites', () => {
    //    +------------------------+
    //  8 | .  .  .  k  .  .  .  . |
    //  7 | .  .  .  .  .  .  .  . |
    //  6 | .  .  .  .  .  .  .  . |
    //  5 | .  .  .  .  P  p  P  . |
    //  4 | .  .  .  .  .  .  .  . |
    //  3 | .  .  .  .  .  .  .  . |
    //  2 | .  .  .  .  .  .  .  . |
    //  1 | .  .  .  K  .  .  .  . |
    //    +------------------------+
    //      a  b  c  d  e  f  g  h

    const fen = '3k4/8/8/4PpP1/8/8/8/3K4 w - f6 0 1';
    const coach = new CoachTester(fen);
    coach.annotate();

    expect(coach.fieldOn('f5').attackers).toHaveLength(2);
    expect(coach.fieldOn('f5').attackers)
      .toContainEqual(coach.relationTo('e5'));
      expect(coach.fieldOn('f5').attackers)
        .toContainEqual(coach.relationTo('g5'));

    expect(coach.fieldOn('e5').attacks)
      .toContainEqual(coach.relationTo('f5'));
    expect(coach.fieldOn('g5').attacks)
      .toContainEqual(coach.relationTo('f5'))
  });

});
