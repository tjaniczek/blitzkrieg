import CoachTester from '../CoachTester';

describe('Coach on: Pawn attacks', () => {
  let coach;

  beforeEach(() => {
    //    +------------------------+
    //  8 | .  .  k  .  .  .  .  . |
    //  7 | p  .  .  .  .  .  p  . |
    //  6 | .  .  .  .  .  .  .  p |
    //  5 | .  .  .  P  .  .  .  . |
    //  4 | .  .  .  .  .  .  .  . |
    //  3 | .  .  .  .  .  N  .  . |
    //  2 | .  .  .  .  .  .  P  . |
    //  1 | .  .  .  .  .  .  K  . |
    //    +------------------------+
    //      a  b  c  d  e  f  g  h

    const fen = '2k5/p5p1/7p/3P4/8/5N2/6P1/6K1 w - - 0 1';
    coach = new CoachTester(fen);
    coach.annotate();
  });

  test('should annotate black pawn attaks', () => {
    expect(coach.fieldOn('a7').attacks)
      .toContainEqual(coach.relationTo('b6'));

    expect(coach.fieldOn('b6').attackers)
      .toContainEqual(coach.relationTo('a7'));
  });

  test('should annotate black pawn defences', () => {
    expect(coach.fieldOn('g7').defends)
      .toContainEqual(coach.relationTo('h6'));
    expect(coach.fieldOn('h6').defenders)
      .toContainEqual(coach.relationTo('g7'));
  });

  test('should annotate white pawn attaks', () => {
    expect(coach.fieldOn('d5').attacks)
      .toContainEqual(coach.relationTo('c6'));

    expect(coach.fieldOn('d5').attacks)
      .toContainEqual(coach.relationTo('e6'));

    expect(coach.fieldOn('c6').attackers)
      .toContainEqual(coach.relationTo('d5'));

    expect(coach.fieldOn('e6').attackers)
      .toContainEqual(coach.relationTo('d5'));
  });

  test('should annotate white pawn defences', () => {
    expect(coach.fieldOn('g2').defends)
      .toContainEqual(coach.relationTo('f3'));
    expect(coach.fieldOn('f3').defenders)
      .toContainEqual(coach.relationTo('g2'));
  });

  test('should annotate proper number of attacks and defences', () => {
    expect(coach.fieldOn('a7').attacks).toHaveLength(1);
    expect(coach.fieldOn('a7').defends).toHaveLength(0);

    expect(coach.fieldOn('g7').attacks).toHaveLength(1);
    expect(coach.fieldOn('g7').defends).toHaveLength(1);

    expect(coach.fieldOn('d5').attacks).toHaveLength(2);
    expect(coach.fieldOn('d5').defends).toHaveLength(0);

    expect(coach.fieldOn('g2').attacks).toHaveLength(1);
    expect(coach.fieldOn('g2').defends).toHaveLength(1);
  });

});
