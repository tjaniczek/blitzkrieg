import CoachTester from '../CoachTester';
import { Direction } from '../Pieces';

describe('Coach on: Absolute pins', () => {
  test('when bishop pins the king', () => {
    //    +------------------------+
    //  8 | .  k  .  .  .  .  .  . |
    //  7 | .  .  .  .  .  .  .  . |
    //  6 | .  .  .  .  .  .  .  . |
    //  5 | .  .  .  .  n  .  .  . |
    //  4 | .  .  .  .  .  B  .  . |
    //  3 | .  .  .  .  .  .  .  . |
    //  2 | .  .  .  .  .  .  .  . |
    //  1 | .  .  .  .  .  K  .  . |
    //    +------------------------+
    //      a  b  c  d  e  f  g  h
    const fen = '1k6/8/8/4n3/5B2/8/8/5K2 w - - 0 1';
    const coach = new CoachTester(fen);
    coach.annotate();
    expect(coach.fieldOn('e5').tactics.absolutePin)
      .toBe(Direction.LeftDiagonal);
  });

  test('when queen pins the king on diagonal', () => {
    //    +------------------------+
    //  8 | .  k  .  .  .  .  .  . |
    //  7 | .  .  .  .  .  .  .  . |
    //  6 | .  .  .  .  .  .  .  . |
    //  5 | .  .  .  .  n  .  .  . |
    //  4 | .  .  .  .  .  Q  .  . |
    //  3 | .  .  .  .  .  .  .  . |
    //  2 | .  .  .  .  .  .  .  . |
    //  1 | .  .  .  .  .  K  .  . |
    //    +------------------------+
    //      a  b  c  d  e  f  g  h
    const fen = '1k6/8/8/4n3/5Q2/8/8/5K2 w - - 0 1';
    const coach = new CoachTester(fen);
    coach.annotate();
    expect(coach.fieldOn('e5').tactics.absolutePin)
      .toBe(Direction.LeftDiagonal);
  });

  test('when rook pins the king', () => {
    //    +------------------------+
    //  8 | k  .  .  .  .  .  .  . |
    //  7 | .  .  .  .  r  .  .  . |
    //  6 | .  .  .  .  .  .  .  . |
    //  5 | .  .  .  .  .  .  .  . |
    //  4 | .  .  .  .  .  .  .  . |
    //  3 | .  .  .  .  Q  .  .  . |
    //  2 | .  .  .  .  .  .  .  . |
    //  1 | .  .  .  .  K  .  .  . |
    //    +------------------------+
    //      a  b  c  d  e  f  g  h

    const fen = 'k7/4r3/8/8/8/4Q3/8/4K3 w - - 0 1';
    const coach = new CoachTester(fen);
    coach.annotate();
    expect(coach.fieldOn('e3').tactics.absolutePin)
      .toBe(Direction.File);
  });

  test('when queen pins the king on straight', () => {
      //    +------------------------+
      //  8 | k  .  .  .  .  .  .  . |
      //  7 | .  .  .  .  q  .  .  . |
      //  6 | .  .  .  .  N  .  .  . |
      //  5 | .  .  .  .  .  .  .  . |
      //  4 | .  .  .  .  .  .  .  . |
      //  3 | .  .  .  .  .  .  .  . |
      //  2 | .  .  .  .  .  .  .  . |
      //  1 | .  .  .  .  K  .  .  . |
      //    +------------------------+
      //      a  b  c  d  e  f  g  h

    const fen = 'k7/4q3/4N3/8/8/8/8/4K3 w - - 0 1';
    const coach = new CoachTester(fen);
    coach.annotate();
    expect(coach.fieldOn('e6').tactics.absolutePin)
      .toBe(Direction.File);

  });

  test('when rook or queen pins the king on row', () => {
      //    +------------------------+
      //  8 | .  .  .  .  .  .  .  . |
      //  7 | .  k  p  .  .  .  Q  . |
      //  6 | .  .  .  .  .  .  .  . |
      //  5 | .  .  .  .  .  .  .  . |
      //  4 | .  .  .  .  .  .  .  . |
      //  3 | .  .  .  .  .  .  .  . |
      //  2 | .  K  .  B  .  r  .  . |
      //  1 | .  .  .  .  .  .  .  . |
      //    +------------------------+
      //      a  b  c  d  e  f  g  h

    const fen = '8/1kp3Q1/8/8/8/8/1K1B1r2/8 w - - 0 1';
    const coach = new CoachTester(fen);
    coach.annotate();
    expect(coach.fieldOn('d2').tactics.absolutePin)
      .toBe(Direction.Row);
      expect(coach.fieldOn('c7').tactics.absolutePin)
        .toBe(Direction.Row);
  });

  test.skip('should annotate absolute pin for rook and en passant', () => {
    //    +------------------------+
    //  8 | .  .  .  .  k  .  .  . |
    //  7 | .  .  .  .  .  .  .  . |
    //  6 | .  .  .  .  .  .  .  . |
    //  5 | .  r  .  P  p  .  .  K |
    //  4 | .  .  .  .  .  .  .  . |
    //  3 | .  .  .  .  .  .  .  . |
    //  2 | .  .  .  .  .  .  .  . |
    //  1 | .  .  .  .  .  .  .  . |
    //    +------------------------+
    //      a  b  c  d  e  f  g  h

    const fen = '4k3/8/8/1r1Pp2K/8/8/8/8 w - e6 0 1';
    // const fen = '4k3/8/8/1r1P3K/8/8/8/8 w - e6 0 1';
    const coach = new CoachTester(fen);
    coach.annotate();

    expect(coach.fieldOn('d5').tactics.enPassantBlocked)
      .toBe(true);
  });
});
