import CoachTester from '../CoachTester';

describe('Coach on: Pins', () => {
  describe('using bishop', () => {
    let coach;
    beforeEach(() => {
      //   +------------------------+
      // 8 | .  .  .  .  k  .  .  . |
      // 7 | p  .  .  .  .  .  .  . |
      // 6 | .  q  .  .  .  .  .  r |
      // 5 | .  .  .  .  .  .  p  . |
      // 4 | .  .  .  n  .  .  .  . |
      // 3 | .  .  .  .  B  .  .  . |
      // 2 | .  .  .  .  .  .  .  . |
      // 1 | .  .  .  .  K  .  .  . |
      //   +------------------------+
      //     a  b  c  d  e  f  g  h

      const fen = '4k3/p7/1q5r/6p1/3n4/4B3/8/4K3 w - - 0 1';
      coach = new CoachTester(fen);
      coach.annotate();
    });

    test('should annotate pieces pinned by bishop', () => {
      expect(coach.fieldOn('d4').tactics.pins).toContainEqual({
        attacker: coach.relationTo('e3'),
        target: coach.relationTo('b6'),
      });

      expect(coach.fieldOn('g5').tactics.pins).toContainEqual({
        attacker: coach.relationTo('e3'),
        target: coach.relationTo('h6'),
      });
    });

    test('should pin only one piece', () => {
      expect(coach.fieldOn('d4').tactics.pins).not.toContainEqual({
        attacker: coach.relationTo('e3'),
        target: coach.relationTo('a7'),
      });
    });
  });

  describe('using queen on diagonal', () => {
    let coach;
    beforeEach(() => {
      //   +------------------------+
      // 8 | .  .  .  .  k  .  .  . |
      // 7 | p  .  .  .  .  .  .  . |
      // 6 | .  q  .  .  .  .  .  r |
      // 5 | .  .  .  .  .  .  p  . |
      // 4 | .  .  .  n  .  .  .  . |
      // 3 | .  .  .  .  Q  .  .  . |
      // 2 | .  .  .  .  .  .  .  . |
      // 1 | .  .  .  .  K  .  .  . |
      //   +------------------------+
      //     a  b  c  d  e  f  g  h

      const fen = '4k3/p7/1q5r/6p1/3n4/4Q3/8/4K3 w - - 0 1';
      coach = new CoachTester(fen);
      coach.annotate();
    });

    test('should annotate pieces pinned by queen', () => {
      expect(coach.fieldOn('d4').tactics.pins).toContainEqual({
        attacker: coach.relationTo('e3'),
        target: coach.relationTo('b6'),
      });

      expect(coach.fieldOn('g5').tactics.pins).toContainEqual({
        attacker: coach.relationTo('e3'),
        target: coach.relationTo('h6'),
      });
    });

    test('should pin only one piece', () => {
      expect(coach.fieldOn('d4').tactics.pins).not.toContainEqual({
        attacker: coach.relationTo('e3'),
        target: coach.relationTo('a7'),
      });
    });
  });

  describe('using rook', () => {
    let coach;
    beforeEach(() => {
    //    +------------------------+
    //  8 | .  .  .  .  .  .  .  k |
    //  7 | .  .  .  .  .  .  .  . |
    //  6 | .  .  .  .  .  .  .  . |
    //  5 | q  p  .  R  .  p  p  b |
    //  4 | .  .  .  n  .  .  .  . |
    //  3 | .  .  .  r  .  .  .  . |
    //  2 | .  .  .  .  .  .  .  . |
    //  1 | .  K  B  .  .  .  .  r |
    //    +------------------------+
    //      a  b  c  d  e  f  g  h

      const fen = '7k/8/8/qp1R1ppb/3n4/3r4/8/1KB4r w - - 0 1';
      coach = new CoachTester(fen);
      coach.annotate();
    });

    test('should annotate pieces pinned by rook', () => {
      expect(coach.fieldOn('b5').tactics.pins).toContainEqual({
        attacker: coach.relationTo('d5'),
        target: coach.relationTo('a5')
      });
      expect(coach.fieldOn('f5').tactics.pins).toContainEqual({
        attacker: coach.relationTo('d5'),
        target: coach.relationTo('g5')
      });
      expect(coach.fieldOn('d4').tactics.pins).toContainEqual({
        attacker: coach.relationTo('d5'),
        target: coach.relationTo('d3')
      });
    });

    test('should annotate pieces pinned from edge', () => {
      expect(coach.fieldOn('c1').tactics.pins).toContainEqual({
        attacker: coach.relationTo('h1'),
        target: coach.relationTo('b1')
      });
    });

    test('should pin only one piece', () => {
      expect(coach.fieldOn('f5').tactics.pins).not.toContainEqual({
        attacker: coach.relationTo('d5'),
        target: coach.relationTo('h5')
      });
    });
  });

  describe('using queen on straight', () => {
    let coach;
    beforeEach(() => {
    //    +------------------------+
    //  8 | .  .  .  .  .  .  .  k |
    //  7 | .  .  .  .  .  .  .  . |
    //  6 | .  .  .  .  .  .  .  . |
    //  5 | q  p  .  Q  .  p  p  b |
    //  4 | .  .  .  n  .  .  .  . |
    //  3 | .  .  .  r  .  .  .  . |
    //  2 | .  .  .  .  .  .  .  . |
    //  1 | .  K  B  .  .  .  .  r |
    //    +------------------------+
    //      a  b  c  d  e  f  g  h

      const fen = '7k/8/8/qp1Q1ppb/3n4/3r4/8/1KB4r w - - 0 1';
      coach = new CoachTester(fen);
      coach.annotate();
    });

    test('should annotate pieces pinned by queen', () => {
      expect(coach.fieldOn('b5').tactics.pins).toContainEqual({
        attacker: coach.relationTo('d5'),
        target: coach.relationTo('a5')
      });
      expect(coach.fieldOn('f5').tactics.pins).toContainEqual({
        attacker: coach.relationTo('d5'),
        target: coach.relationTo('g5')
      });
      expect(coach.fieldOn('d4').tactics.pins).toContainEqual({
        attacker: coach.relationTo('d5'),
        target: coach.relationTo('d3')
      });
    });

    test('should annotate pieces pinned from edge', () => {
      expect(coach.fieldOn('c1').tactics.pins).toContainEqual({
        attacker: coach.relationTo('h1'),
        target: coach.relationTo('b1')
      });
    });

    test('should pin only one piece', () => {
      expect(coach.fieldOn('f5').tactics.pins).not.toContainEqual({
        attacker: coach.relationTo('d5'),
        target: coach.relationTo('h5')
      });
    });
  });
});
