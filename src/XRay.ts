import { Chess } from 'chess.js';
import Board from './Board';
import {
  Piece,
  Pieces,
  Direction,
} from './Pieces';

import {
  Field,
  SpecialMoves,
  PieceRelation,
  XRayRelation } from './types';

export default class XRay {
  public coach: any;

  constructor(coach) {
    this.coach = coach;
  }

  annotate(attacker:Field, vector: Field[], direction:Direction) {
    let hasTarget: boolean = false;
    let hasXRayAttack: boolean = false;
    let firstTarget:Field;

    vector.map((target) => {
      if (!hasTarget) {
        this.coach.annotateRelation(attacker, target);
      }

      if (target.piece) {
        if (hasTarget && !hasXRayAttack) {
          this.annotateXRayAttack(attacker, target, firstTarget, direction);
          hasXRayAttack = true;
        }

        if (!hasTarget) {
          firstTarget = target;
          hasTarget = true;
        }
      };
    });

    return vector;
  }

  annotateXRayAttack(attacker:Field, target:Field, middle:Field, direction:Direction) {
    if (attacker.piece.color == target.piece.color) {
      return;
    }

    const attackerCoordinates:PieceRelation = {
      row: attacker.row,
      file: attacker.file,
      piece: attacker.piece,
    };

    const targetCoordinates:PieceRelation = {
      row: target.row,
      file: target.file,
      piece: target.piece,
    };

    const xRay:XRayRelation = {
      attacker: attackerCoordinates,
      target: targetCoordinates,
    };

    if (middle.piece.color === attacker.piece.color) {
      this.annotateDoubleAttacks(attacker, middle, target, xRay);
    } else {
      this.annotatePinnedAttacks(attacker, middle, target, xRay, direction);
    }
  }

  annotateDoubleAttacks(attacker, middle, target, xRay) {
    const attackerType = attacker.piece.id;
    const middleType = middle.piece.id;

    if (this.isPawnBattery(attacker, middle, target)) {
      middle.tactics.batteries.push(xRay);
    } else if (this.isBattery(attacker, middle)) {
      middle.tactics.batteries.push(xRay);
    } else if (middle.piece.id === Piece.Pawn){
      this.annotatePawnCannon(attacker, middle, xRay);
    } else {
      middle.tactics.cannons.push(xRay);
    }
  }

  annotatePawnCannon(attacker, middle, xRay) {
    if (attacker.piece.straight) {
      if (this.isAttacking(middle)) {
        middle.tactics.cannons.push(xRay);
      }
    }

    if (attacker.piece.diagonal) {
      const moves = this.coach.chessJs.moves({ square: middle.label });
      if (moves.length) {
        middle.tactics.cannons.push(xRay);
      }
    }
  }

  annotatePinnedAttacks(attacker, middle, target, xRay, direction) {
      if (middle.piece.value <= target.piece.value) {
        middle.tactics.pins.push(xRay);
        if (target.piece.id === Piece.King) {
          middle.tactics.absolutePin = direction;
        }
      } else {
        middle.tactics.skewers.push(xRay);
      }
  }

  isAttacking(field:Field):boolean {
    return field.attacks.filter(field => field.piece).length > 0;
  }

  isBattery(first, second) {
    const diagonalBattery = first.piece.diagonal && second.piece.diagonal;
    const straightBattery = first.piece.straight && second.piece.straight;

    return diagonalBattery || straightBattery;
  }

  isPawnBattery(attacker, middle, target) {
    const middleIsPawn = middle.piece.id == Piece.Pawn;

    if (!middleIsPawn) {
      return undefined;
    }

    if (!attacker.piece.diagonal) {
      return undefined;
    }

    return this.isPawnNextToTarget(middle, target);
  }

  isPawnNextToTarget(pawn, target) {
    const distance = pawn.row - target.row;
    return distance === 1 || distance === -1;
  }

  // annotateEnPassantPins(attacker:Field, vector: Field[]) {
  //   if (!this.specialMoves.enPassant) {
  //     return undefined;
  //   }
  //   if (attacker.piece.id !== Piece.Rook) {
  //     return undefined;
  //   }
  //
  //   // const kingOnLine = vector.map(field => field.piece ? field.piece.id : undefined).includes(Piece.King);
  //   const attackLine = vector.filter(field => field.piece);
  //   console.log(attackLine)
  // }
}
