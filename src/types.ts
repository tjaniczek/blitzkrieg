 interface PieceInfo {
  id: number;
  label: string;
  value: number;
  diagonal?: boolean;
  straight?: boolean;
  color?: string;
  type?: string;
}

interface Coordinate {
  file: number;
  row: number;
  label: string;
}

interface Castling {
  kingSide?: boolean;
  queenSide?: boolean;
}

interface SpecialMoves {
  enPassant?: Coordinate;
  whiteCastling: Castling;
  blackCastling: Castling;
}

interface PieceRelation {
  file: number;
  row: number;
  piece?: PieceInfo;
}

interface XRayRelation {
  attacker: PieceRelation;
  target: PieceRelation;
}

interface Tactics {
  pins: XRayRelation[];
  skewers: XRayRelation[];
  cannons: XRayRelation[];
  batteries: XRayRelation[];
  absolutePin: boolean;
  enPassant: boolean;
  enPassantBlocked: boolean;
}

interface Field {
  file: number;
  row: number;
  label: string;
  attackers: PieceRelation[];
  defenders: PieceRelation[];
  piece?: PieceInfo;
  attacks: PieceRelation[];
  defends: PieceRelation[];
  tactics: Tactics;
}

export {
  PieceInfo,
  SpecialMoves,
  PieceRelation,
  XRayRelation,
  Field,
}
